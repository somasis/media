# Copyright 2008 Bryan Østergaard
# Copyright 2008, 2010 David Vazgenovich Shakaryan <dvshakaryan@gmail.com>
# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="An ncurses based client for MPD"
HOMEPAGE="https://www.musicpd.org/clients/${PN}"
DOWNLOADS="https://www.musicpd.org/download/ncmpc/$(ever major)/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.62]
        dev-python/Sphinx [[ note = [ man pages and html documentation ] ]]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/pcre
        media-libs/libmpdclient[>=2.9]
        sys-libs/ncurses
    suggestion:
        media-sound/mpd[>=0.19] [[ description = [ Ncmpc connects to an mpd daemon, either locally or on a remote host ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dasync_connect=true
    -Dchat_screen=true
    -Dcolors=true
    -Dcurses=ncursesw
    -Ddocumentation=enabled
    -Dhelp_screen=true
    -Dhtml_manual=false
    -Dkey_screen=true
    -Dlibrary_screen=true
    -Dlirc=disabled
    -Dlocale=enabled
    -Dlyrics_plugin_dir=/usr/$(exhost --target)/lib/${PN}/lyrics
    -Dlyrics_screen=true
    -Dmanual=true
    -Dmini=false
    -Dmouse=enabled
    -Dmultibyte=true
    -Dnls=enabled
    -Doutputs_screen=true
    -Dregex=enabled
    -Dsearch_screen=true
    -Dsong_screen=true
)

