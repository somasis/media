# Copyright 2011-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_EXTERNAL_REFS="thirdparty/libcrashreporter-qt:"
fi

require github [ user=${PN}-player ]
require cmake freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="The Social Music Player"
HOMEPAGE="http://${PN}-player.org"

BUGS_TO="tgurr@exherbo.org"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-cpp/sparsehash
        kde-frameworks/extra-cmake-modules[>=1.7.0]
        virtual/pkg-config
    build+run:
        app-arch/quazip[>=0.4.3][qt5(+)]
        app-crypt/qca:2[qt5(+)] [[ note = [ library and qca-ossl plugin ] ]]
        dev-cpp/lucene++[>=3.0.0]
        dev-libs/boost[>=1.3]
        dev-libs/gnutls
        kde-frameworks/attica:5[>=1.0.0]
        media/vlc[>=2.1.0]
        media-libs/liblastfm[>=1.0.0][qt5(+)]
        media-libs/phonon[>=4.5.0][qt5(+)]
        media-libs/taglib[>=1.9]
        net-im/telepathy-qt[>=0.9.3][qt5(+)]
        net-libs/jreen[>=1.1.1][qt5(+)]
        sys-auth/qtkeychain[>=0.1.0][qt5(+)]
        x11-libs/libX11
        x11-libs/qtbase:5[sql][sqlite] [[ note = [ Qt5Concurrent Qt5Core Qt5DBus Qt5Widgets Qt5Xml ] ]]
        x11-libs/qtsvg:5 [[ note = [ Qt5Svg ] ]]
        x11-libs/qttools:5 [[ note = [ Qt5LinguistTools Qt5UiTools ] ]]
        x11-libs/qtwebkit:5 [[ note = [ Qt5WebKitWidgets ] ]]
        x11-libs/qtx11extras:5 [[ note = [ Qt5X11Extras ] ]]
    run:
        sys-apps/upower
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_GUI:BOOL=TRUE
    -DBUILD_HATCHET:BOOL=FALSE
    -DBUILD_WITH_QT4:BOOL=FALSE
    -DWITH_BINARY_ATTICA:BOOL=TRUE
    -DWITH_GNOMESHORTCUTHANDLER:BOOL=TRUE
    -DWITH_KDE4:BOOL=FALSE
    -DWITH_UPOWER:BOOL=TRUE
)

tomahawk_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

tomahawk_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

