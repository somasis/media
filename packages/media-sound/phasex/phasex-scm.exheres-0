# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 1.11 ] ]

SUMMARY="Phase Harmonic Advanced Synthesis EXperiment"
DESCRIPTION="
PHASEX is a software synthesizer designed to incorporate some experimental
features not found in many other synths (like phase offset modulation) along
with a good implementation of a lot of the features seen on most modern
synthesizers.
"
HOMEPAGE="https://github.com/disabled/phasex-dev"
DOWNLOADS=""
SCM_REPOSITORY="git://github.com/disabled/phasex-dev.git"
require scm-git

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="lash"

DEPENDENCIES="
    build+run:
        media-libs/libsamplerate[>=0.1.2]
        media-sound/jack-audio-connection-kit[>=0.99.0]
        sys-sound/alsa-lib[>=0.9.0]
        x11-libs/libX11
        x11-libs/gtk+:2[>=2.4]
        lash? ( media-sound/lash[>=0.5.4] )
"

BUGS_TO="alip@exherbo.org"

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'lash' )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/ROADMAP )

src_prepare() {
    # Phasex fails at link-time when gmodule libs aren't present.
    # TODO: Report upstream!
    edo sed -i \
        -e 's/@PHASEX_LIBS@/& $(shell pkg-config --libs gmodule-2.0)/' \
        src/Makefile.am

    eautoreconf
}

