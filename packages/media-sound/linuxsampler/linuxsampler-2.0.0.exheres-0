# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Based in part upon linux-sampler-9999.ebuild which is:
#   Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Software audio sampler engine with professional grade features"
HOMEPAGE="http://www.linuxsampler.org/"
DOWNLOADS="https://download.linuxsampler.org/packages/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="alsa doc jack
    instruments-db [[ description = [ Enable instruments database ] ]]
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
        sys-devel/bison
        virtual/pkg-config
    build+run:
        media-libs/dssi
        media-libs/libgig[>=4.0.0]
        media-libs/liblscp
        media-libs/libsndfile[>=1.0][flac][vorbis]
        media-libs/lv2[>=1.0.0]
        alsa?   ( sys-sound/alsa-lib )
        jack?   ( media-sound/jack-audio-connection-kit )
        instruments-db? ( dev-db/sqlite:3[>=3.3] )
"

BUGS_TO="alip@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-sf2-engine
    --disable-arts-driver
    --disable-asio-driver
    --disable-coreaudio-driver
    --disable-coremidi-driver
    --disable-midishare-driver
    --disable-mmemidi-driver
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'alsa alsa-driver'
    'jack jack-driver'
    'instruments-db'
)

src_compile() {
    default

    if option doc; then
        emake docs
    fi
}

src_install() {
    default

    if option doc; then
        edo cd doc
        dodoc -r html
    fi

    keepdir /usr/$(exhost --target)/lib/${PN}/plugins
}

