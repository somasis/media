# Copyright 2009 Ingmar Vanhassel
# Copyright 2015 Thomas G. Anderson <tanderson@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A shared library to access the contents of an iPod"
HOMEPAGE="http://gtkpod.sourceforge.net/"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    aac
    cd [[ description = [ Build Sound Juicer plugin to support ripping CDs ] ]]
    cover-art [[ description = [ Build Clarity and Coverweb plugins to display and fetch coverart ] ]]
    flac
    media-player [[ description = [ Use gstreamer to play tracks inside gtkpod ] ]]
    ogg
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.33]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.31]
        dev-libs/libxml2:2.0[>=2.7.7]
        gnome-desktop/libanjuta[>=2.91][glade]
        gnome-desktop/gdl:3.0[>=3.0.0]
        gnome-platform/libglade:2[>=2.4.0]
        media-libs/libgpod[>=0.7.0]
        media-libs/libid3tag[>=0.15]
        net-misc/curl[>=7.10.0]
        x11-libs/clutter-gtk:1.0[>=1.2]
        x11-libs/gdk-pixbuf:2.0[>=2.31.3]
        x11-libs/gtk+:3[>=3.0.8]
        aac? ( media-libs/faad2 )
        cd? (
            app-text/iso-codes
            gnome-desktop/brasero[>=3.0]
            media-libs/libdiscid[>=0.2.2]
            media-libs/gstreamer:1.0[>=1.0]
            media-plugins/gst-plugins-base:1.0[>=1.0]
            media-libs/libmusicbrainz:5[>=5.0.1]
            flac? ( media-plugins/gst-plugins-good[gstreamer_plugins:flac] )
            ogg? ( media-plugins/gst-plugins-base[gstreamer_plugins:vorbis][gstreamer_plugins:ogg] )
        )
        cover-art? (
            x11-libs/clutter-gtk:1.0[>=1.2.0]
        )
        flac? ( media-libs/flac[>=1.2.1] )
        media-player? (
            media-libs/gstreamer:1.0[>=1.0]
            media-plugins/gst-plugins-base:1.0[>=1.0]
        )
        ogg? ( media-libs/libvorbis[>=1.3.1] )
    run:
        aac? ( media-libs/libmp4v2 ) [[ note = [ libmp4v2 is loaded dynamically at runtime and not needed at compile time ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-curl # Support for downloading Cover-art
    --disable-plugin-coverweb
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'cd plugin-sjcd'
    'cover-art plugin-clarity'
    'media-player plugin-media-player'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'aac mp4' flac ogg )

