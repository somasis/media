# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="A set of well-supported plugins with questionable licenses"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gstreamer_plugins:
        a52      [[ description = [ ATSC A/52 audio decoding using a52dec ] ]]
        amr      [[ description = [ Adaptive Multi Rate encoder/decoder (narrow band, wide band) ] ]]
        cdio     [[ description = [ Read audio CDs using libcdio ] ]]
        dvdread  [[ description = [ Access DVD title/chapter/angle using libdvdread ] ]]
        h264     [[ description = [ H.264/AVC/MPEG-4 part 10 encoding using the x264 library ] ]]
        mpeg2    [[ description = [ MPEG video stream decoding using libmpeg2 ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.16]
        media-libs/gstreamer:1.0[>=${PV}]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        gstreamer_plugins:a52? ( media-libs/a52dec )
        gstreamer_plugins:amr? ( media-libs/opencore-amr[>=0.1.3] )
        gstreamer_plugins:cdio? ( dev-libs/libcdio[>=0.76] )
        gstreamer_plugins:dvdread? ( media-libs/libdvdread[>=0.5.0] )
        gstreamer_plugins:h264? ( media-libs/x264[>=0.120] )
        gstreamer_plugins:mpeg2? ( media-libs/libmpeg2[>=0.5.1] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dorc=enabled

    # Core plugins
    -Dasfdemux=enabled
    -Ddvdlpcmdec=enabled
    -Ddvdsub=enabled
    -Drealmedia=enabled
    -Dxingmux=enabled

    # Missing libsidplay
    -Dsidplay=disabled

    -Dnls=enabled

    -Dglib-asserts=disabled
    -Dglib-checks=disabled
    -Dgobject-cast-checks=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gstreamer_plugins:a52 a52dec'
    'gstreamer_plugins:amr amrnb'
    'gstreamer_plugins:amr amrwbdec'
    'gstreamer_plugins:cdio'
    'gstreamer_plugins:dvdread'
    'gstreamer_plugins:h264 x264'
    'gstreamer_plugins:mpeg2 mpeg2dec'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

