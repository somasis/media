# Copyright 2010 Paul Seidler <sepek@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="A library for facilitating the creation of audio/video editors."
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        dev-libs/glib:2[>=2.40]
        media-libs/gstreamer:1.0[>=${PV}][gobject-introspection?]
        media-plugins/gst-plugins-base:1.0[>=${PV}][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
    test:
        media-plugins/gst-plugins-bad:1.0 [[
            note = [ It's not required but the meson script doesn't handle it correctly when it wasn't found ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Disable deprecated formatter
    -Dxptv=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

