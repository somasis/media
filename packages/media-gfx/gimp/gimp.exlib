# Copyright 2008 Richard Brown
# Copyright 2010 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011,2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_{prepare,configure,install} pkg_{preinst,postinst,postrm}

SUMMARY="GNU Image Manipulation Program"
HOMEPAGE="https://www.gimp.org"
DOWNLOADS="https://download.gimp.org/pub/gimp/v$(ever range 1-2)/${PNV}.tar.bz2"

LICENCES="GPL-3 LGPL-3"
SLOT="0"
MYOPTIONS="
    aalib
    alsa
    gtk-doc
    hdr [[ description = [ Support for the OpenEXR image file format ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    jpeg2000
    mng
    postscript [[ description = [ Support importing PS (PostScript) files ] ]]
    python
    webp [[ description = [ Support for the Webp image format ] ]]
    wmf [[ description = [ Support for the Windows Metafile image format ] ]]

    platform: amd64
    x86_cpu_features: mmx sse
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: am ar ast az be bg br bs ca ca@valencia cs csb da de dz el en_CA en_GB eo es et eu
               fa fi fr ga gd gl gu he hi hr hu id is it ja ka kk km kn ko ky lt lv mk ml mr ms my
               nb nds ne nl nn oc pa pl pt pt_BR ro ru rw si sk sl sr sr@latin sv ta te th tr tt uk
               vi xh yi zh_CN zh_HK zh_TW )
"

# tests attempt to use X and fail
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools [[ note = [ autotools regeneration ] ]]
        dev-lang/perl:*[>=5.10.0]
        dev-util/intltool[>=0.40.1]
        sys-devel/gettext[>=0.19]
        virtual/pkg-config[>=0.16]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        app-arch/xz[>=5.0.0]
        app-text/iso-codes
        app-text/poppler[>=0.50.0][cairo][glib]
        app-text/poppler-data[>=0.4.7]
        dev-libs/atk[>=2.2.0]
        dev-libs/gexiv2[>=0.10.6]
        dev-libs/glib:2[>=2.54.2]
        dev-libs/libunwind
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        gnome-desktop/libgudev[>=167]
        gnome-desktop/librsvg:2[>=2.40.6]
        graphics/mypaint-brushes
        media-libs/babl[>=0.1.72]
        media-libs/fontconfig[>=2.12.4]
        media-libs/freetype:2[>=2.1.7]
        media-libs/gegl:0.4[>=0.4.18]
        media-libs/tiff
        media-libs/lcms2[>=2.8]
        media-libs/libmypaint[>=1.3.0]
        media-libs/libpng:=[>=1.6.25]
        sys-libs/zlib
        x11-libs/cairo[>=1.12.2] [[ note = [ needs cairo-pdf, which we have enabled by default ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.30.8]
        x11-libs/gtk+:2[>=2.24.32]
        x11-libs/harfbuzz[>=0.9.19]
        x11-libs/libX11
        x11-libs/libXcursor [[ note = [ hard-enabled because gtk+ needs it anyway ] ]]
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXmu
        x11-libs/libXpm [[ note = [ hard-enabled because people who have xorg-server or xterm have it installed anyway and it's small ] ]]
        x11-libs/pango[>=1.29.4]
        aalib? ( media-libs/aalib )
        alsa? ( sys-sound/alsa-lib[>=1.0.0] )
        hdr? ( media-libs/openexr[>=1.6.1] )
        heif? ( media-libs/libheif[>=1.3.2] )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.1.0] )
        mng? ( media-libs/libmng )
        postscript? ( app-text/ghostscript )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        python? (
            dev-lang/python:*[>=2.5]
            dev-python/pycairo[>=1.0.2]
            gnome-bindings/pygtk:2[>=2.10.4]
        ) [[ note = [ needs to be tested with python3 ] ]]
        webp? ( media-libs/libwebp:=[>=0.6.0] )
        wmf? ( media-libs/libwmf[>=0.2.8] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=167] )
    run:
        x11-themes/hicolor-icon-theme
    suggestion:
        gnome-desktop/gvfs [[ description = [ Use gvfs to access remote files using the URI plugin ] ]]
"
# configure checks for gegl:matting-levi, which will be enabled for
# gegl[>=0.4.14], but it's only a runtime dependency.

gimp_src_prepare() {
    # TODO: fix upstream
    edo sed -e "/AC_PATH_PROG(PKG_CONFIG/d" -i m4macros/gimp-2.0.m4

    # TODO: fix upstream
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:" tools/gimptool.c

    # TODO: fix upstream, respect datarootdir
    # can't use intoolize because gimg has a strange po handling
    edo sed -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i "${WORK}"/po*/Makefile.in.in

    autotools_src_prepare
}

gimp_src_configure() {
    local myconf=(
        # required to build invert-svg.c
        CC_FOR_BUILD=$(exhost --build)-cc
        --enable-default-binary
        --enable-mp
        --enable-nls
        --with-print
        --with-xmc
        # Alternative to libunwind in order to get detailed traces
        --without-libbacktrace
        --without-webkit
        # This could maybe be used to enable tests, if it was packaged
        --without-xvfb-run
        $(option_enable gtk-doc)
        $(option_enable python)
        $(option_with aalib aa)
        $(option_with alsa)
        $(option_with hdr openexr)
        $(option_with heif libheif)
        $(option_with jpeg2000)
        $(option_with mng libmng)
        $(option_with postscript gs)
        $(option_with webp)
        $(option_with wmf)
    )

    # If I don't do this, they are forced off
    if option platform:amd64 ; then
        myconf+=(
            --enable-mmx
            --enable-sse
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
        )
    fi

    if [[ $(exhost --target) == *-musl* ]] ; then
        myconf+=(
            --without-libunwind
        )
    else
        myconf+=(
            # Could be optional, but it's small and usefull
            --with-libunwind
        )
    fi

    econf "${myconf[@]}"
}

gimp_src_install() {
    default

    keepdir /usr/share/gimp/2.0/fonts
}

gimp_pkg_preinst() {
    # Make update from <=2.10.4 to >=2.10.6 work.
    # Since 79961a654559e06cd853a4337bd1e97e963ec080 plugins are installed in
    # a directory named after them, causing "Cannot overwrite file foo with
    # directory foo" errors.
    if has_version 'media-gfx/gimp[<=2.10.4]' ; then
        for dir in $(find "${IMAGE}"/usr/$(exhost --target)/lib/gimp/2.0/plug-ins -type d) ; do
            nonfatal edo rm "${ROOT}"/usr/$(exhost --target)/lib/gimp/2.0/plug-ins/$(basename ${dir})
        done
    fi
}

gimp_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

gimp_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

