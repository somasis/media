# Copyright 2009, 2010 Sterling X. Winter <replica@exherbo.org>
# Copyright 2009 Michael Forney <michael@obberon.com>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ branch=$(ever range 1-2).x suffix=tar.bz2 ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Scalable vector graphics editor"
DESCRIPTION="
Inkscape is an Open Source vector graphics editor, with capabilities similar to
Illustrator, CorelDraw, or Xara X, using the W3C standard Scalable Vector
Graphics (SVG) file format. Inkscape supports many advanced SVG features
(markers, clones, alpha blending, etc.) and great care is taken in designing a
streamlined interface. It is very easy to edit nodes, perform complex path
operations, trace bitmaps and much more.
"
HOMEPAGE+=" https://inkscape.org"

UPSTREAM_RELEASE_NOTES="http://wiki.inkscape.org/wiki/index.php/Release_notes/${PV}"

LICENCES="GPL-2 GPL-3 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cdr [[ description = [ Enable support for importing CorelDraw files ] ]]
    dia [[ description = [ Enable support for importing Dia files ] ]]
    gnome [[ description = [ Use gnome-vfs for file loading ] ]]
    lcms [[ description = [ Enable LittleCMS for color management ] ]]
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    pdf [[ description = [ Enable PDF previews using poppler ] ]]
    postscript [[ description = [ Enable support for importing PostScript files ] ]]
    visio [[ description = [ Enable support for importing Microsoft Visio files ] ]]
    wpg [[ description = [ Enable support for importing WordPerfect Graphics files ] ]]
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Software suites to create, edit, and compose bitmap images ]
        number-selected = at-least-one
    ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# Tests fail to run (last checked: 0.92.2)
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-util/intltool
        sys-devel/gettext[>=0.19]
        virtual/pkg-config
    build+run:
        app-spell/aspell                         [[ note = [ Automagic dep ] ]]
        app-spell/gtkspell:2                     [[ note = [ Automagic dep ] ]]
        dev-cpp/cairomm:1.0[>=1.9.8]
        dev-cpp/libsigc++:2[>=2.0.12]
        dev-libs/atk                             [[ note = [ Automagic dep ] ]]
        dev-libs/boehm-gc[>=7.1]
        dev-libs/boost[>=1.36]
        dev-libs/glib:2[>=2.28]
        dev-libs/libxml2:2.0[>=2.6.11]
        dev-libs/libxslt[>=1.0.15]
        dev-libs/popt
        gnome-bindings/atkmm:1.6
        gnome-bindings/glibmm:2.4[>=2.32][-disable-deprecated]
        gnome-bindings/gtkmm:2.4[>=2.10.0]
        gnome-bindings/pangomm:1.4
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libexif                       [[ note = [ Automagic dep ] ]]
        media-libs/libpng:=[>=1.2]
        sci-libs/gsl                             [[ note = [ Automagic dep ] ]]
        sys-libs/zlib                            [[ note = [ Automagic dep ] ]]
        x11-libs/cairo[>=1.10]                   [[ note = [ Automagic dep ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2[>=2.24]
        x11-libs/libX11                          [[ note = [ Automagic dep ] ]]
        x11-libs/libXft
        x11-libs/pango[>=1.24]                   [[ note = [ Automagic dep ] ]]
        gnome? ( gnome-platform/gnome-vfs[>=2.0] )
        cdr? (
            media-libs/libcdr[>=0.03]
            office-libs/librevenge
        )
        lcms? ( media-libs/lcms2[>=1.13] )
        openmp? ( sys-libs/libgomp:= )
        pdf? ( app-text/poppler[>=0.20.0][cairo][glib] )
        visio? (
            media-libs/libvisio[>=0.0.20]
            office-libs/librevenge
        )
        wpg? (
            office-libs/libwpg[>=0.3]
            office-libs/librevenge
        )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[>=1.0.6] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:imagemagick? ( media-gfx/ImageMagick[>=1.0.6] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    test:
        dev-cpp/gtest[>=1.8.0][googlemock]
    run:
        dia? ( app-diagram/dia )
        postscript? ( media-gfx/pstoedit )
    suggestion:
        dev-python/lxml [[ description = [ Required for using some of the templates ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.92.2-Use-CMAKE-INSTALL-DATAROOTDIR-part1.patch
    "${FILES}"/${PN}-0.92.2-Use-CMAKE-INSTALL-DATAROOTDIR-part2.patch
    "${FILES}"/e831b034746f8dc3c3c1b88372751f6dcb974831.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Potrace:BOOL=TRUE
    -DPACKAGE_LOCALE_DIR:PATH=/usr/share/locale
    -DENABLE_BINRELOC:BOOL=FALSE
    -DWITH_DBUS:BOOL=TRUE
    -DWITH_IMAGE_MAGICK:BOOL=TRUE
    -DWITH_GTK3_EXPERIMENTAL:BOOL=FALSE
    -DWITH_LPETOOL:BOOL=FALSE
    -DWITH_NLS:BOOL=TRUE
    -DWITH_PROFILING:BOOL=FALSE
    -DWITH_SVG2:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'lcms LCMS'
    'pdf POPPLER'
    'pdf POPPLER_CAIRO'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'cdr LIBCDR'
    'gnome GNOME_VFS'
    'openmp OPENMP'
    'visio LIBVISIO'
    'wpg LIBWPG'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DCMAKE_DISABLE_FIND_PACKAGE_GTest:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_GTest:BOOL=TRUE'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( TRANSLATORS )

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

