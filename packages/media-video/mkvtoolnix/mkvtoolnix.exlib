# Copyright 2015 Xavier Barrachina <xv.barrachina@gmail.com>
# Copyright 2009, 2010 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_compile src_test src_install pkg_postinst pkg_postrm

SUMMARY="MKVToolnix is a set of tools to create, alter and inspect Matroska files under Linux"
HOMEPAGE="https://${PN}.download"
DOWNLOADS="${HOMEPAGE}/sources/${PNV}.tar.xz"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    flac
    qt5
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-lang/ruby:*[>=2.0.0]
        dev-libs/json[>=3.5.0]
        dev-libs/libxslt
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/boost[>=1.49.0]
        dev-libs/fmt[>=5.3.0]
        dev-libs/pugixml[>=1.9]
        media-libs/libebml[>=1.3.7]
        media-libs/libmatroska[>=1.5.0]
        media-libs/libogg
        media-libs/libvorbis
        sys-apps/file
        sys-libs/zlib
        flac? ( media-libs/flac )
        qt5? (
            app-text/cmark
            x11-libs/qtbase:5[>=5.9.0][gui]
            x11-libs/qtmultimedia:5[>=5.9.0]
        )
    test:
        dev-cpp/gtest[>=1.8.0]
    suggestion:
        media/mediainfo[gui] [[
            description = [ Support MediaInfo for displaying additional information ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-optimization
    --disable-static
    --disable-update-check
    --with-gettext
    --with-qt-pkg-config
    --without-po4a
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'qt5 qt' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( flac )

mkvtoolnix_src_compile() {
    edo rake V=1 -m -j${EXJOBS:-1}
}

mkvtoolnix_src_test() {
    edo rake V=1 -m -j${EXJOBS:-1} tests:run_unit
}

mkvtoolnix_src_install() {
    edo rake DESTDIR="${IMAGE}" install
}

mkvtoolnix_pkg_postinst() {
    option qt5 && freedesktop-desktop_pkg_postinst
    option qt5 && freedesktop-mime_pkg_postinst
    option qt5 && gtk-icon-cache_pkg_postinst
}

mkvtoolnix_pkg_postrm() {
    option qt5 && freedesktop-desktop_pkg_postrm
    option qt5 && freedesktop-mime_pkg_postrm
    option qt5 && gtk-icon-cache_pkg_postrm
}

