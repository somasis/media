# Copyright 2015-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kodi-9999.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation

export_exlib_phases pkg_postinst pkg_postrm

require github [ user=xbmc pn=xbmc ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache \
    python [ blacklist=3 multibuild=false has_bin=true python_opts=[sqlite] ]

SUMMARY="Kodi is an award-winning free and open source software media center"
HOMEPAGE="https://${PN}.tv"

DOWNLOADS+="
    https://github.com/xbmc/libdvdcss/archive/${LIBDVDCSS_REV}.tar.gz -> libdvdcss-${LIBDVDCSS_REV}.tar.gz
    https://github.com/xbmc/libdvdread/archive/${LIBDVDREAD_REV}.tar.gz -> libdvdread-${LIBDVDREAD_REV}.tar.gz
    https://github.com/xbmc/libdvdnav/archive/${LIBDVDNAV_REV}.tar.gz -> libdvdnav-${LIBDVDNAV_REV}.tar.gz
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    airplay [[ description = [ Enable support for Airplay, wireless local media streaming ] ]]
    alsa
    avahi
    bluetooth
    nfs [[ description = [ Access remote media via NFS ] ]]
    pulseaudio
    samba [[ description = [ Access remote media via samba ] ]]
    sync [[ description = [ Enable synching multiple Kodi installations via MySQL ] ]]
    va [[ description = [ Enable hardware Video Acceleration (VA API) ] ]]
    vdpau [[ description = [ Enable hardware Video Acceleration (VDPAU) for NVidia GPUs ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Doesn't find the kodi-test executable
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/doxygen
        dev-lang/swig[>=2.0]
        dev-util/gperf
        sys-devel/cmake
        virtual/jre:=[>=1.6]
    build+run:
        app-arch/lzo:2
        app-arch/unzip
        app-arch/zip
        dev-db/sqlite:3
        dev-libs/crossguid
        dev-libs/expat
        dev-libs/flatbuffers[>=1.9.0]
        dev-libs/fmt[>=3.0.1]
        dev-libs/fribidi
        dev-libs/fstrcmp
        dev-libs/libcdio[>=0.78]
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        dev-libs/pcre
        dev-libs/rapidjson[>=1.0.2]
        dev-libs/tinyxml[>=2.6.2]
        dev-python/Pillow[>=3.0.0][python_abis:*(-)?]
        fonts/dejavu
        media/ffmpeg[>=4.0.4][h264]
        media-libs/freetype:2
        media-libs/giflib:=[>=4.1.6] [[ note = [ for TexturePacker ] ]]
        media-libs/glew
        media-libs/lcms2
        media-libs/libass[>=0.9.8]
        media-libs/libpng:= [[ note = [ for TexturePacker ] ]]
        media-libs/taglib[>=1.9]
        media-libs/tiff [[ note = [ for TexturePacker ] ]]
        media-sound/lame
        net-libs/libmicrohttpd[>=0.9.40]
        net-misc/curl
        sys-apps/dbus
        sys-apps/util-linux [[ note = [ libuuid ] ]]
        sys-libs/libcap
        sys-libs/zlib
        x11-dri/libdrm[>=2.4.82]
        x11-dri/mesa [[ note = [ provides GBM and GLX ] ]]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXmu
        x11-libs/libXrandr
        x11-libs/libXt
        airplay? ( dev-libs/libplist )
        alsa? ( sys-sound/alsa-lib[>=1.0.27] )
        avahi? ( net-dns/avahi )
        bluetooth? ( net-wireless/bluez )
        nfs? ( net-fs/libnfs[>=2.0.0] )
        pulseaudio? ( media-sound/pulseaudio[>=2.0] )
        samba? ( net-fs/samba )
        sync? ( virtual/mysql )
        va? ( x11-libs/libva[>=0.39.0] )
        vdpau? ( x11-libs/libvdpau )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        providers:systemd? ( sys-apps/systemd )
    run:
        !media/xbmc [[
            description = [ XBMC was renamed to Kodi upstream ]
            resolution = uninstall-blocked-after
        ]]
    suggestion:
        media-plugins/kodi-pvr-hts [[
            description = [ Kodi PVR client Tvheadend HTSP ]
        ]]
        media-plugins/kodi-pvr-iptvsimple [[
            description = [ Kodi PVR client IPTV Simple ]
        ]]
        media-plugins/kodi-pvr-vdr-vnsi [[
            description = [ Kodi PVR client VDR VNSI ]
        ]]
        sys-apps/udisks:2 [[
            description = [ Removable media support ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DENABLE_AML:BOOL=FALSE
    -DENABLE_BLURAY:BOOL=FALSE
    -DENABLE_CAP:BOOL=TRUE
    -DENABLE_CCACHE:BOOL=FALSE
    -DENABLE_CEC:BOOL=FALSE
    -DENABLE_DBUS:BOOL=TRUE
    -DENABLE_DVDCSS:BOOL=TRUE
    -DENABLE_EVENTCLIENTS:BOOL=FALSE
    -DENABLE_INTERNAL_CROSSGUID:BOOL=FALSE
    -DENABLE_INTERNAL_FFMPEG:BOOL=FALSE
    -DENABLE_INTERNAL_FLATBUFFERS:BOOL=FALSE
    -DENABLE_INTERNAL_FMT:BOOL=FALSE
    -DENABLE_INTERNAL_FSTRCMP:BOOL=FALSE
    -DENABLE_INTERNAL_RapidJSON:BOOL=FALSE
    -DENABLE_LCMS2:BOOL=TRUE
    -DENABLE_LDGOLD:BOOL=FALSE
    -DENABLE_LIRCCLIENT:BOOL=FALSE
    -DENABLE_MARIADBCLIENT:BOOL=FALSE
    -DENABLE_MDNS:BOOL=FALSE
    -DENABLE_MICROHTTPD:BOOL=TRUE
    -DENABLE_OPENGL:BOOL=TRUE
    -DENABLE_OPTICAL:BOOL=TRUE
    -DENABLE_PYTHON:BOOL=TRUE
    -DENABLE_SNDIO:BOOL=FALSE
    -DENABLE_UDEV:BOOL=TRUE
    -DENABLE_UPNP:BOOL=TRUE
    -DENABLE_X:BOOL=TRUE
    -DENABLE_XSLT:BOOL=TRUE
    -DLIBDVDREAD_URL="${FETCHEDDIR}"/libdvdread-${LIBDVDREAD_REV}.tar.gz
    -DLIBDVDNAV_URL="${FETCHEDDIR}"/libdvdnav-${LIBDVDNAV_REV}.tar.gz
    -DLIBDVDCSS_URL="${FETCHEDDIR}"/libdvdcss-${LIBDVDCSS_REV}.tar.gz
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
    -DPYTHON_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/python${PYTHON_ABIS}
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'airplay AIRTUNES'
    'airplay PLIST'
    'alsa ALSA'
    'avahi AVAHI'
    'bluetooth BLUETOOTH'
    'nfs NFS'
    'pulseaudio PULSEAUDIO'
    'samba SMBCLIENT'
    'sync MYSQLCLIENT'
    'va VAAPI'
    'vdpau VDPAU'
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

kodi_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kodi_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

