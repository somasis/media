# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require toolchain-funcs option-renames [ renames=[ 'v4l2 v4l' 'vp8 vpx' ] ]

export_exlib_phases pkg_setup src_configure

SUMMARY="A complete solution to record, convert and stream audio and video"
DESCRIPTION="
FFmpeg is a complete solution to record, convert and stream audio and video. It includes libavcodec,
the leading audio/video codec library. The project is made of several components:

    * ffmpeg is a command line tool to convert one video file format to another
      as well as grabbing and encoding in real time from a TV card.
    * ffplay is a simple media player based on SDL and on the FFmpeg libraries.
    * libavcodec is a library containing all the FFmpeg audio/video
      encoders and decoders.
    * libavformat is a library containing parsers and generators for all common
      audio/video formats.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.bz2"

BUGS_TO="kimrhh@exherbo.org"

UPSTREAM_CHANGELOG="https://git.videolan.org/?p=${PN}.git;a=shortlog;h=n${PV} [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://git.videolan.org/?p=${PN}.git;a=blob;f=RELEASE_NOTES;hb=release/$(ever range 1-2) [[ lang = en ]]"

GPL_OPTIONS=(
    cd
    frei0r
    hevc
    providers:openh264
    providers:x264
    rubberband
    samba
    teletext
    vidstab
    xavs
)

V3_OPTIONS=(
    opencore
    samba
)

if ever at_least 4.1; then
    V3_OPTIONS+=(
        lensfun
        providers:mbedtls
    )
fi

# TODO: somehow express that according to configure they are only nonfree if combined with gpl
# options
NONFREE_OPTIONS=(
    fdk-aac
    nvenc
    providers:libressl
    providers:openssl
)

LICENCES="LGPL-2.1"

for opt in "${GPL_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( GPL-2 )"
done
for opt in "${V3_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( LGPL-3 )"
done

# TODO: the nasm/yasm dep requires platform:, but that's not reliable when cross-compiling
SLOT="0"
MYOPTIONS="
    fdk-aac      [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library, makes the resulting binary non-redistributable ] ]]
    alsa         [[ description = [ Support sound recoding and playback using ALSA ] ]]
    ass          [[ description = [ Advanced SubStation Alpha (ASS) subtitles support ] ]]
    bidi         [[ description = [ Enable support for text shaping in drawtext via libfribidi ] ]]
    bluray       [[ description = [ Support for reading Blu-ray discs ] ]]
    bs2b         [[ description = [ Enable support for libbs2b-based stereo-to-binaural audio filter ] ]]
    caca         [[ description = [ Support for colored ASCII video output ] ]]
    cd           [[ description = [ Audio CD grabbing ] ]]
    celt         [[ description = [ Support for the CELT low-delay audio codec ] ]]
    doc
    fontconfig   [[ description = [ Support for managing custom fonts via fontconfig ] ]]
    frei0r       [[ description = [ Video effects using frei0r-plugins ] ]]
    gsm          [[ description = [ Support for GSM codec (audio), mainly for telephony ] ]]
    h264         [[ description = [ Enable H.264 encoding ] ]]
    h264? ( ( providers:
        openh264 [[ description = [ Enable H.264 encoding using openh264 ] ]]
        x264 [[ description = [ Enable H.264 encoding using x264 ] ]]
    ) [[ number-selected = at-least-one ]] )
    hap          [[ description = [ Hap video encoder and decoder ] ]]
    hevc         [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    ieee1394     [[ description = [ Enable IIDC-1394 grabbing using libdc1394 ] ]]
    ilbc         [[ description = [ Support for decoding/encoding the Internet Low Bitrate Codec (iLBC) audio codec ] ]]
    jack
    jpeg2000     [[ description = [ Support for decoding/encoding lossy image compression format ] ]]
    kms          [[ description = [ KMS screen grabber input device ] ]]
    ladspa       [[ description = [ Linux Audio Developer Simple Plugin audio filtering support ] ]]
    lv2          [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
    modplug      [[ description = [ ModPlug support, for MOD-like music files ] ]]
    mp2          [[ description = [ Enable MPEG Audio Layer 2 encoding through twolame ] ]]
    mp3          [[ description = [ Support for mp3 encoding with lame ] ]]
    nvenc        [[ description = [ Enable NVIDIA Encoder (NVENC) API for hardware accelerated H.264 and H.265/HEVC encoding ] ]]
    openal       [[ description = [ OpenAL capture support ] ]]
    opencl       [[ description = [ Hardware acceleration via OpenCL ] ]]
    opencore     [[ description = [ Support for OpenCORE AMR-WB decoder and encoder (audio) ] ]]
    opencv       [[ description = [ Computer Vision techniques from OpenCV ] note = [ untested ] ]]
    opengl       [[ description = [ OpenGL rendering output ] ]]
    opus         [[ description = [ Enable support for Opus de/decoding via libopus ] ]]
    player       [[ requires = [ sdl ] description = [ FFmpeg reference audio and video player software ] ]]
    pulseaudio   [[ description = [ Pulseaudio capture support ] ]]
    rtmp         [[ description = [ RTMP (flash stream) support ] ]]
    rubberband   [[ description = [ Filter for time-stretching and pitch-shifting using librubberband ] ]]
    samba        [[ description = [ Enable support for the Samba protocol via libsmbclient ] ]]
    sdl          [[ description = [ Support output through SDL ] ]]
    sftp         [[ description = [ SFTP protocol support via libssh ] ]]
    sndio        [[ description = [ Adds support for recording and playback through sndio (OpenBSD sound API, also ported to Linux) ] ]]
    speex        [[ description = [ Enable support for decoding and encoding audio using libspeex ] ]]
    svg
    teletext     [[ description = [ Teletext support via libzvbi ] ]]
    theora       [[ description = [ Enable support for encoding using the Theora Video Compression Codec ] ]]
    truetype
    v4l          [[ description = [ Access V4L2 devices ] ]]
    va           [[ description = [ Enable support for decoding video using the Video Acceleration API ] ]]
    vdpau        [[ requires = [ X ] description = [ Enable support for VDPAU hardware accelerated video decoding ] ]]
    vidstab      [[ description = [ Analyze and perform video stabilization/deshaking ] ]]
    vorbis       [[ description = [ Additional OggVorbis audio de-/encoder plugin (ffmpeg's encoder is experimental) ] ]]
    vpx          [[ description = [ Enable support for VP7/VP8 and VP9 de/encoding via libvpx ] ]]
    webp         [[ description = [ WebP encoding via libwebp ] ]]
    X            [[ description = [ Enable support for X11 grabbing ] ]]
    xavs         [[ description = [ Support AVS, the Audio Video Standard of China ] ]]
    xml          [[ description = [ XML parsing using the C library libxml2 ] ]]
    xv           [[ description = [ Enable XVideo output, so ffmpeg can e.g. display the video while encoding ] ]]

    ( platform: amd64 x86 )
"

for opt in "${NONFREE_OPTIONS[@]}"; do
    # We repeat the bindist [[ ]] part so the suboption prefix is only valid for the one option
    # we're handling at the moment. Otherwise we'd need to require a specific order in
    # NONFREE_OPTIONS. ()-grouping doesn't work here.
    if [[ "${opt}" == *:* ]]; then
        MYOPTIONS+=" bindist [[ requires = [ ${opt%:*}: -${opt#*:} ] ]]"
    else
        MYOPTIONS+=" bindist [[ requires = [ -${opt} ] ]]"
    fi
done

DEPENDENCIES="
    build:
        sys-apps/texinfo
        virtual/pkg-config
        nvenc? ( dev-libs/nvidia-video-codec-sdk[>=0.5] )
        opencl? ( dev-libs/opencl-headers )
        platform:amd64? ( dev-lang/nasm )
        platform:x86? ( dev-lang/nasm )
    build+run:
        app-arch/bzip2
        sys-libs/zlib
        alsa? ( sys-sound/alsa-lib )
        ass? ( media-libs/libass )
        bidi? ( dev-libs/fribidi )
        bluray? ( media-libs/libbluray )
        bs2b? ( media-libs/libbs2b )
        caca? ( media-libs/libcaca )
        cd? ( dev-libs/libcdio-paranoia )
        celt? ( media-libs/celt:0.11 )
        fdk-aac? ( media-libs/fdk-aac )
        fontconfig? ( media-libs/fontconfig )
        frei0r? ( media-plugins/frei0r-plugins )
        gsm? ( media-libs/gsm )
        hap? ( app-arch/snappy )
        hevc? ( media-libs/x265:=[>=1.8] )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394
        )
        ilbc? ( media-libs/ilbc )
        jack? ( media-sound/jack-audio-connection-kit )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.1] )
        kms? ( x11-dri/libdrm )
        ladspa? ( media-libs/ladspa-sdk )
        lv2? (
            media-libs/lilv
            media-libs/lv2
        )
        modplug? ( media-libs/libmodplug )
        mp2? ( media-libs/twolame[>=0.3.10] )
        mp3? ( media-sound/lame[>=3.98.3] )
        openal? ( media-libs/openal )
        opencl? ( dev-libs/ocl-icd )
        opencore? (
            media-libs/opencore-amr
            media-libs/vo-amrwbenc
        )
        opencv? ( media-libs/opencv )
        opengl? ( x11-dri/mesa )
        opus? ( media-libs/opus )
        providers:gnutls? (
            dev-libs/gmp:=
            dev-libs/gnutls
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openh264? ( media-libs/openh264[>=1.3] )
        providers:openssl? ( dev-libs/openssl )
        providers:x264? ( media-libs/x264[>=20120104] )
        pulseaudio? ( media-sound/pulseaudio )
        rtmp? ( media-video/rtmpdump[>=2.2] )
        rubberband? ( media-libs/rubberband[>=1.8.1] )
        samba? ( net-fs/samba )
        sdl? ( media-libs/SDL:2[>=2.0.1] )
        sftp? ( net-libs/libssh )
        sndio? ( sys-sound/sndio )
        speex? ( media-libs/speex )
        svg? (
            gnome-desktop/librsvg:2
            x11-libs/cairo
        )
        teletext? ( media-libs/zvbi )
        theora? (
            media-libs/libogg
            media-libs/libtheora
        )
        truetype? ( media-libs/freetype:2 )
        v4l? ( media-libs/v4l-utils )
        va? ( x11-libs/libva[>=1.0.6] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vidstab? ( media-libs/libvidstab[>=0.98] )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
        vpx? ( media-libs/libvpx:=[>=1.3.0] )
        webp? ( media-libs/libwebp:=[>=0.4.0] )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXv
            x11-libs/libxcb[>=1.4]
        )
        xavs? ( media-libs/xavs )
        xml? ( dev-libs/libxml2:2.0 )
        xv? ( x11-libs/libXv )
        !media/libav [[ description = [ Libav is a fork of ffmpeg and uses the same file names ] ]]
"

if ever at_least 4.1; then
    MYOPTIONS+="
        lensfun [[ description = [ Use the lensfun library for correcting optical lens defects ] ]]
        ( providers:
            gnutls
            libressl [[ description = [ Use LibreSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
            mbedtls
            openssl  [[ description = [ Use OpenSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
        ) [[ number-selected = exactly-one ]]
    "
    DEPENDENCIES+="
        build+run:
            lensfun? ( media-libs/lensfun )
            providers:mbedtls? ( dev-libs/mbedtls )
    "
else
    MYOPTIONS+="
        ( providers:
            gnutls
            libressl [[ description = [ Use LibreSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
            openssl  [[ description = [ Use OpenSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
        ) [[ number-selected = exactly-one ]]
    "
fi

if ever at_least 4.2; then
    MYOPTIONS+="
        av1 [[ description = [ AV1 decoding via libdav1d ] ]]
    "
    DEPENDENCIES+="
        build+run:
            app-arch/xz
            av1? ( media-libs/dav1d[>=0.2.1] )
    "
fi

DEFAULT_SRC_INSTALL_PARAMS=( install-man )

FFMPEG_LIBDIRS=(
    libavcodec
    libavdevice
    libavfilter
    libavformat
    libavresample
    libavutil
    libpostproc
    libswresample
    libswscale
)

DEFAULT_SRC_TEST_PARAMS=( LD_LIBRARY_PATH=$(IFS=:; echo "${FFMPEG_LIBDIRS[*]}" ) fate )

ffmpeg-cc_define_enable() {
    cc-has-defined ${1} && echo --enable-${2} || echo --disable-${2}
}

ffmpeg_pkg_setup() {
    export V=1
}

# grayscale support is disabled
ffmpeg_src_configure() {
    local myconf=() target=$(exhost --target)

    # licences
    local opt gpl_param="" v3_param="" nonfree_param=""

    for opt in "${GPL_OPTIONS[@]}"; do
        option "${opt}" && gpl_param="--enable-gpl"
    done
    for opt in "${V3_OPTIONS[@]}"; do
        option "${opt}" && v3_param="--enable-version3" # LGPL-3 or GPL-3 depending on gpl_param
    done
    for opt in "${NONFREE_OPTIONS[@]}"; do
        option "${opt}" && nonfree_param="--enable-nonfree" # not redistributable
    done

    myconf+=(
        ${gpl_param}
        ${v3_param}
        ${nonfree_param}
    )

    #FIXME this configure script doesn't handle --host
    myconf+=(
        --ar="${AR}"
        --nm="${NM}"
        --cc="${CC}"
        --host-cflags="${CFLAGS}" # respect user cflags and avoid -O3 -g
        --cxx="${CXX}"
        --extra-cxxflags="${CXXFLAGS}" # we have currently no other way to use user flags
        --prefix=/usr/$(exhost --target)
        --pkg-config="${PKG_CONFIG}"
        --ranlib="${RANLIB}"
        --datadir=/usr/share/${PN}
        --docdir=/usr/share/doc/${PNVR}
        --mandir=/usr/share/man

        ### CPU features
        --arch=${target%%-*}

        # x86
        # nasm/yasm requires mmx, as configure doesn't properly set OPTS if mmx is disabled
        # this is quite nice as we won't enable nasm/yasm on non-x86 as side-effect
        $(ffmpeg-cc_define_enable __MMX__ x86asm)
        $(ffmpeg-cc_define_enable __MMX__ mmx)
        # most of ffmpeg's mmxext functions have an sse2 alternative with higher priority anyways
        # $(ffmpeg-cc_define_enable TODO mmxext)
        $(ffmpeg-cc_define_enable __3dNOW__ amd3dnow)
        # ffmpeg uses pfpnacc which is part of 3dNOW_A according to gcc sources
        $(ffmpeg-cc_define_enable __3dNOW_A__ amd3dnowext)
        $(ffmpeg-cc_define_enable __AESNI__ aesni)
        $(ffmpeg-cc_define_enable __AVX__ avx)
        $(ffmpeg-cc_define_enable __AVX2__ avx2)
        $(ffmpeg-cc_define_enable __AVX512__ avx512)
        $(ffmpeg-cc_define_enable __FMA__ fma3)
        $(ffmpeg-cc_define_enable __FMA4_ fma4)
        $(ffmpeg-cc_define_enable __SSE__ sse)
        $(ffmpeg-cc_define_enable __SSE2__ sse2)
        $(ffmpeg-cc_define_enable __SSE3__ sse3)
        $(ffmpeg-cc_define_enable __SSSE3__ ssse3)
        $(ffmpeg-cc_define_enable __SSE4_1__ sse4)
        $(ffmpeg-cc_define_enable __SSE4_2__ sse42)
        $(ffmpeg-cc_define_enable __XOP__ xop)

        # arm
        $(ffmpeg-cc_define_enable __VFP_FP__ vfp)
        $(ffmpeg-cc_define_enable __ARM_NEON neon)
        # $(ffmpeg-cc_define_enable TODO vfp3)
        # $(ffmpeg-cc_define_enable TODO armv5te)
        # $(ffmpeg-cc_define_enable TODO armv6)
        # $(ffmpeg-cc_define_enable TODO armv6t2)
        $(ffmpeg-cc_define_enable __ARM_ARCH_8A armv8)

        ### hard enable

        # system related
        --enable-bzlib
        --enable-iconv
        --enable-lzma
        --enable-lzo
        --enable-shared
        --enable-pthreads
        --enable-zlib
        --disable-autodetect
        --disable-debug
        --disable-linux-perf
        --disable-optimizations # optimizations is just a stupid way of enabling -O3 -fomit-frame-pointer
        --disable-static
        --disable-stripping

        # prefer librtmp for rtmp(t)e support
        --disable-gcrypt
        --disable-gmp
        # prefer the native encoder of FFmpeg over using libxvid
        --disable-libxvid

        # wrong platform, but disable autodetect
        --disable-amf
        --disable-appkit
        --disable-audiotoolbox
        --disable-avfoundation
        --disable-coreimage
        --disable-d3d11va
        --disable-dxva2
        --disable-jni
        --disable-mediacodec
        --disable-schannel
        --disable-securetransport
        --disable-videotoolbox

        # missing dependency
        --disable-cuvid
        --disable-decklink
        --disable-ffnvcodec
        --disable-libaom
        --disable-libcodec2
        --disable-libflite
        --disable-libgme
        --disable-libkvazaar
        --disable-libmfx
        --disable-libmysofa
        --disable-libnpp
        --disable-libopenmpt
        --disable-libshine
        --disable-libsoxr
        --disable-libsrt
        --disable-libtls
        --disable-libvmaf
        --disable-libzimg
        --disable-mmal
        --disable-omx
        --disable-omx-rpi
        --disable-rkmpp

        # not tested
        --disable-chromaprint
        --disable-libtesseract
        --disable-libwavpack
        --disable-libzmq
        --disable-nvdec

        # libav compatibility (deprecated)
        --enable-avresample

        # Breaks pbins
        --disable-runtime-cpudetect

        ### OPTIONS
        $(option_enable ass libass)
        $(option_enable alsa)
        $(option_enable bidi libfribidi)
        $(option_enable bluray libbluray)
        $(option_enable bs2b libbs2b)
        $(option_enable caca libcaca)
        $(option_enable cd libcdio)
        $(option_enable celt libcelt)
        $(option_enable doc)
        $(option_enable doc htmlpages)
        $(option_enable fdk-aac libfdk-aac)
        $(option_enable fontconfig)
        $(option_enable fontconfig libfontconfig)
        $(option_enable frei0r)
        $(option_enable gsm libgsm)
        $(option_enable hap libsnappy)
        $(option_enable hevc libx265)
        $(option_enable ieee1394 libdc1394)
        $(option_enable ilbc libilbc)
        $(option_enable jack libjack)
        $(option_enable jpeg2000 libopenjpeg)
        $(option_enable kms libdrm)
        $(option_enable ladspa)
        $(option_enable lv2)
        $(option_enable modplug libmodplug)
        $(option_enable mp2 libtwolame)
        $(option_enable mp3 libmp3lame)
        $(option_enable nvenc)
        $(option_enable openal)
        $(option_enable opencl)
        $(option_enable opencore libopencore-amrnb)
        $(option_enable opencore libopencore-amrwb)
        $(option_enable opencore libvo-amrwbenc)
        $(option_enable opencv libopencv)
        $(option_enable opengl)
        $(option_enable opus libopus)
        $(option_enable player ffplay)
        $(option_enable providers:openh264 libopenh264)
        $(option_enable providers:x264 libx264)
        $(option_enable pulseaudio libpulse)
        $(option_enable rtmp librtmp)
        $(option_enable rubberband librubberband)
        $(option_enable samba libsmbclient)
        $(option_enable sdl sdl2)
        $(option_enable sftp libssh)
        $(option_enable sndio)
        $(option_enable speex libspeex)
        $(option_enable svg librsvg)
        $(option_enable teletext libzvbi)
        $(option_enable theora libtheora)
        $(option_enable truetype libfreetype)
        $(option_enable v4l libv4l2)
        $(option_enable v4l v4l2-m2m)
        $(option_enable va vaapi)
        $(option_enable vdpau)
        $(option_enable vidstab libvidstab)
        $(option_enable vorbis libvorbis)
        $(option_enable vpx libvpx)
        $(option_enable webp libwebp)
        $(option_enable X libxcb)
        $(option_enable X libxcb-shm)
        $(option_enable X libxcb-xfixes)
        $(option_enable X libxcb-shape)
        $(option_enable X xlib)
        $(option_enable xavs libxavs)
        $(option_enable xml libxml2)
        $(option alsa || echo "--disable-indev=alsa")
        $(option alsa || echo "--disable-outdev=alsa")
        $(option xv || echo "--disable-outdev=xv")
    )

    if ever at_least 4.1; then
        myconf+=(
            # missing dependency
            --disable-libdavs2
            --disable-libklvanc
            --disable-libtensorflow
            --disable-libxavs2
            --disable-vapoursynth

            ### OPTIONS
            $(option_enable lensfun liblensfun)
            $(option_enable providers:mbedtls mbedtls)
        )
    fi

    if ever at_least 4.2; then
        myconf+=(
            # missing dependency
            --disable-cuda-nvcc
            --disable-libaribb24
            --disable-pocketsphinx

            # not tested
            --disable-cuda-llvm

            ### OPTIONS
            $(option_enable av1 libdav1d)
        )
    else
        myconf+=(
            # missing dependency
            --disable-cuda-sdk
            --disable-libndi_newtek
        )
    fi

    myconf+=(
        $(option_enable providers:gnutls gnutls)
    )
    if option providers:libressl || option providers:openssl ; then
        myconf+=( --enable-openssl )
    else
        myconf+=( --disable-openssl )
    fi

    edo ./configure "${myconf[@]}"
}

