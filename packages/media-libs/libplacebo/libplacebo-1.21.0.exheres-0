# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://code.videolan.org' user='videolan' tag=v${PV} suffix=tar.bz2 ]
require meson

SUMMARY="the core rendering algorithms and ideas of mpv turned into a library"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    lcms [[ description = [ Support for LittleCMS 2 ] ]]
    ( providers: shaderc glslang ) [[
        *description = [ Provide SPIR-V compiler ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/vulkan-headers[>=1.0.42]
        sys-libs/vulkan-loader[>=1.0.42]
        lcms? (
            media-libs/lcms2[>=2.6]
        )
        providers:shaderc? (
            sys-libs/shaderc
        )
        providers:glslang? (
            dev-lang/glslang[>=7.8.2850] [[ note = [ PATCH_LEVEL 2763 ] ]]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbench=false
    -Dvulkan=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    lcms
    'providers:glslang glslang'
    'providers:shaderc shaderc'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_unpack() {
    meson_src_unpack

    # https://gitlab.exherbo.org/exherbo/arbor/issues/5
    edo mv ${PN}-v${PV}-* ${PNV}
}

