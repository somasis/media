# Copyright 2008 Thomas Anderson
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 1.10 ] ]

SUMMARY="A free ATSC A/52 stream decoder"
HOMEPAGE="http://liba52.sourceforge.net"
DOWNLOADS="http://liba52.sourceforge.net/files/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="oss"

DEPENDENCIES="
    build+run:
        dev-libs/djbfft
        oss? ( media-libs/libao )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-optionaltests.patch
    "${FILES}"/${PNV}-cflags.patch
    "${FILES}"/${PNV}-fpic.patch
    "${FILES}"/${PNV}-lto.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--enable-shared"

    # TODO(compnerd) determine what double precision floating point for audio playback via libaudio
    # means; stick with the default of disabled until then.
    "--disable-double"

    # enable DJB FFT fallback for IMDCT
    "--enable-djbfft"

    # platform specific audio backends
    "--disable-solaris-audio"
    "--disable-al-audio"
    "--disable-win"

    # disable profiling support by default
    "--disable-gprof"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    # support OSS via libaudio
    "oss"
)

src_prepare() {
    autotools_src_prepare

    # Use correctly prefixed `nm`
    edo sed -e "/nm -g/s:nm:$(exhost --tool-prefix)nm:" \
            -i test/globals
}

