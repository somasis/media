# Copyright 2013 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion drobilla

SUMMARY="C library to make the use of LV2 plugins as simple as possible for applications"
DESCRIPTION="
Lilv is a C library to make the use of LV2 plugins as simple as possible for
applications. Lilv is the successor to SLV2, rewritten to be significantly
faster and have minimal dependencies. It is stable, well-tested software (the
included test suite covers over 90% of the code) in use by several applications.
"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/serd[>=0.30.0]
        dev-libs/sord[>=0.14.0]
        media-libs/libsndfile[>=1.0.0]
        media-libs/lv2[>=1.16.0]
        media-libs/sratom[>=0.4.0]
"

DROBILLA_SRC_CONFIGURE_PARAMS=(
    --dyn-manifest
    --no-bindings
    --test
)

src_install() {
    waf_src_install

    if option bash-completion; then
        dobashcompletion utils/lilv.bash_completion ${PN}
    fi

    # remove duplicate bash-completion installed to the wrong location
    edo rm -rf "${IMAGE}"/usr/etc
}

