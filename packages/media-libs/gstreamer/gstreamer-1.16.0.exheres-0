# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gstreamer meson

PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS+="
    backtrace [[
        description = [ Generate backtraces using elfutils and libunwind ] ]]
    debug
    gobject-introspection
    gtk-doc
    ( libc: musl )
"

# Test run against / instead of $WORK, which makes them somewhat pointless
RESTRICT="test"

DEPENDENCIES+="
    build:
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        dev-libs/glib:2[>=2.40.0]
        sys-libs/libcap
        backtrace? (
            dev-libs/libunwind
            dev-util/elfutils
            libc:musl? (
                dev-libs/libexecinfo
            )
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgst_parse=true
    -Dregistry=true
    -Dtracer_hooks=true

    -Dextra-checks=true
    -Doption-parsing=true
    -Dpoisoning=true
    -Dmemory-alignment=malloc

    -Dcheck=enabled
    -Dtests=enabled
    -Dtools=enabled

    -Dexamples=disabled
    -Dbenchmarks=disabled

    -Dgobject-cast-checks=disabled
    -Dglib-asserts=disabled
    -Dglib-checks=disabled

    -Dnls=enabled
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'debug gst_debug'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'backtrace libdw'
    'backtrace libunwind'
    'bash-completion'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)

# Needed for async tests
DEFAULT_SRC_TEST_PARAMS=( -j1 )

