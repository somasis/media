# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=badaix tag=v${PV} ] \
    systemd-service [ systemd_files=[ debian/snapserver.service debian/snapclient.service ] ]

SUMMARY="Multi-room client-server audio player"
DESCRIPTION="
Snapcast is a multi-room client-server audio player, where all clients are time synchronized with
the server to play perfectly synced audio. It's not a standalone player, but an extension that
turns your existing audio player into a Sonos-like multi-room solution. The server's audio input is
a named pipe /tmp/snapfifo. All data that is fed into this file will be send to the connected
clients. One of the most generic ways to use Snapcast is in conjunction with the music player
daemon (MPD) or Mopidy, which can be configured to use a named pipe as audio output.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-cpp/asio
        dev-libs/boost[>=1.70.0]
    build+run:
        group/snapserver
        user/snapclient
        user/snapserver
        media-libs/flac
        media-libs/libogg
        media-libs/libvorbis
        net-dns/avahi[dbus]
        sys-sound/alsa-lib
    suggestion:
        dev-rust/librespot[>=0.1.0] [[
            description = [ Spotify Connect support via librepot ]
        ]]
"

src_prepare() {
    edo sed \
        -e 's:-O3:${CFLAGS}:g' \
        -e 's:DEBUG=-g:DEBUG=${CFLAGS}:g' \
        -e '/CXX /d' \
        -e '/STRIP /d' \
        -e '/$(STRIP) /d' \
        -i {client,server}/Makefile

    # https://github.com/badaix/snapcast/pull/8
    edo sed \
        -e 's:/var/run:/run:g' \
        -i server/snapserver.cpp

    edo sed \
        -e 's:/etc/default/snapclient:/etc/conf.d/snapclient.conf:g' \
        -e 's:User=_snapclient:User=snapclient:g' \
        -e 's:Group=_snapclient:Group=audio:g' \
        -i debian/snapclient.service

    edo sed \
        -e 's:/etc/default/snapserver:/etc/conf.d/snapserver.conf:g' \
        -e 's:_snapserver:snapserver:g' \
        -i debian/snapserver.service

    default
}

src_configure() {
    :
}

src_test() {
    :
}

src_install() {
    dobin server/snapserver
    dobin client/snapclient

    doman server/snapserver.1
    doman client/snapclient.1

    insinto /etc
    doins server/etc/snapserver.conf

    insinto /etc/conf.d
    newins debian/snapserver.default snapserver.conf
    newins debian/snapclient.default snapclient.conf

    keepdir /var/lib/snap{client,server}
    edo chown -R snapserver:snapserver "${IMAGE}"/var/lib/snapserver
    edo chown -R snapclient:audio "${IMAGE}"/var/lib/snapclient

    install_systemd_files
}

